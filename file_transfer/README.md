# file_transfer

Python scripts to transfer files between machines using standard libraries.  
2python holds the python2 version of the scripts.  
3python holds the python3 version of the scripts.

## Usage
Start the server and then send the file with the client.  
  
### Server
root@kali:~# python server.py 

### Client
root@kali:~# python client.py 127.0.0.1 5001 file.txt

## Output
#### Server
root@kali:~# python server.py  
[*] Listening as 0.0.0.0:5001  
[+] ('127.0.0.1', 59826) is connected.  
[+] Receiving: file.txt 

#### Client
root@kali:~# python client.py 127.0.0.1 5001 file.txt  
[+] Connecting to 127.0.0.1:5001  
[+] Connected.  

