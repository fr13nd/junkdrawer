# junkdrawer

A collection of random scripts and automation tools.

## Scripts

| Folder/Script  | Description |
| ------------- | ------------- |
| compare_lists/compare.py  | Python script to compare two lists and optionally write the data out to a file  |
| file_transfer/Xpython/client.py  | Python server script to recieve data from the client.py  |
| file_transfer/Xpython/client.py  | Python client script to send data to the server.py  |

## Acknowledgements
file_transfer - Thanks to Abdou Rockikz for his article/code on [How to Transfer Files in the Network using Sockets in Python](https://www.thepythoncode.com/article/send-receive-files-using-sockets-python)