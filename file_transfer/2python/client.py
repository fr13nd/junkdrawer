import socket
import os
import sys

SEPARATOR = "<SEPARATOR>"
BUFFER_SIZE = 4096 # send 4096 bytes each time step

# count the arguments
arguments = len(sys.argv) - 1
if arguments != 3:
    print("")
    print("[!] Error...")
    print("This script takes three arguments.")
    print("Syntax: "+sys.argv[0]+" <IP> <PORT> <FILE>")
    print("Example: "+sys.argv[0]+" 127.0.0.1 9000 data.txt")
    sys.exit()

# set command line variables
host = sys.argv[1]
port = int(sys.argv[2])
filename = sys.argv[3]

# check to make sure the file exists
if str(os.path.isfile(filename)) == "False":
    print("")
    print("[!] Error...")
    print("That file does not exist")
    sys.exit()

# get the file size
filesize = os.path.getsize(filename)

# create the client socket
s = socket.socket()
print("[+] Connecting to "+host+":"+str(port))
s.connect((host, port))
print("[+] Connected.")

# send the filename and filesize
preData = filename+SEPARATOR+str(filesize)
s.send(preData.encode())

# start sending the file
with open(filename, "rb") as f:
    sizeTrack = 0
    while sizeTrack < filesize:
        # read the bytes from the file
        bytes_read = f.read(BUFFER_SIZE)
        if not bytes_read:
            # file transmitting is done
            break
        # we use sendall to assure transimission in busy networks
        s.sendall(bytes_read)

# close the socket
s.close()
