# Make sure both files have the same line endings
# Use dos2unix to convert them both to the same ones

exist_both = []
exist_file1 = []
exist_file2 = []

f = open('file1.txt', 'r')
file1 = f.read().splitlines()
f.close()

f = open('file2.txt', 'r')
file2 = f.read().splitlines()
f.close()

counter = 0
for val in file1:
    print(str(counter)+" "+val)
    if val in file2:
        exist_both.append(val)
        counter+=1
    else:
        exist_file1.append(val)
        counter+=1

print("\n\nOtherside of the list\n\n")

counter = 0
for val in file2:
    if val in file1:
        pass
    else:
        print(str(counter)+" "+val)
        exist_file2.append(val)
        counter+=1

print("\n\n")
print("REPORT")
print("Both:  "+str(len(exist_both)))
print("File1: "+str(len(exist_file1)))
print("File2: "+str(len(exist_file2)))

# # Write out to a file
# with open('output.txt', 'w') as outfile:
#     outfile.writelines("%s\n" % value for value in exist_both)
#     outfile.writelines("%s\n" % value for value in exist_file1)
#     outfile.writelines("%s\n" % value for value in exist_file2)
