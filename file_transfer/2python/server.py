import socket
import os

# device's IP address
SERVER_HOST = "0.0.0.0"
SERVER_PORT = 5001
BUFFER_SIZE = 4096 # receive 4096 bytes each time
SEPARATOR = "<SEPARATOR>"

# create the server socket that will listen for up to 5 connections
s = socket.socket()
s.bind((SERVER_HOST, SERVER_PORT))
s.listen(5)

print("[*] Listening as "+SERVER_HOST+":"+str(SERVER_PORT))


while True:
    client_socket, address = s.accept() 
    print("[+] "+str(address)+" is connected.")

    # receive the file infos
    # receive using client socket, not server socket
    received = client_socket.recv(BUFFER_SIZE).decode()
    filename, filesize = received.split(SEPARATOR)
    filename = os.path.basename(filename)
    filesize = int(filesize)

    # print file being received
    print("[+] Receiving: "+filename)
    print("")

    # start receiving the file from the socket and writing to the file stream
    with open(filename, "wb") as f:
        sizeTrack = 0
        while sizeTrack < filesize:
            # read BUFFER_SIZE bytes from the socket (receive)
            bytes_read = client_socket.recv(BUFFER_SIZE)
            # nothing is received file transmitting is done
            if not bytes_read:    
                break
            # write to the file the bytes we just received
            f.write(bytes_read)

    # close the client socket
    client_socket.close()

# close the server socket
s.close()
